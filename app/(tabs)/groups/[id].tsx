import { StyleSheet } from "react-native";

import ParallaxScrollView from "@/components/ParallaxScrollView";
import { ThemedText } from "@/components/ThemedText";
import { ThemedView } from "@/components/ThemedView";
import { IconSymbol } from "@/components/ui/IconSymbol";
import { getGroupById } from "@/utils/get-group-by-id";
import { getTotalByGroup } from "@/utils/get-total-by-group";
import { Redirect, router, useLocalSearchParams } from "expo-router";
import { Avatar, Card, IconButton } from "react-native-paper";

export default function GroupScreen() {
  const { id } = useLocalSearchParams();

  const group = getGroupById(Number(id) as unknown as number);

  console.log(group);

  if (!group) {
    return <Redirect href="/+not-found" />;
  }

  return (
    <ParallaxScrollView
      headerBackgroundColor={{ light: "#D0D0D0", dark: "#353636" }}
      headerImage={
        <IconSymbol
          size={310}
          color="#808080"
          name="chevron.left.forwardslash.chevron.right"
          style={styles.headerImage}
        />
      }
    >
      <ThemedView style={styles.titleContainer}>
        <IconButton
          icon="arrow-left"
          onPress={() => {
            router.push(
              group.parent ? `/groups/${group.parent_id}` : "/platforms"
            );
          }}
        />
        <ThemedText type="title">{group.name}</ThemedText>
      </ThemedView>
      {group.children.map((group) => (
        <Card.Title
          key={group.id}
          title={group.name}
          subtitle={new Intl.NumberFormat("fr-FR", {
            style: "currency",
            currency: "EUR",
          }).format(getTotalByGroup(group))}
          left={(props) => <Avatar.Icon {...props} icon="folder" />}
          right={(props) => (
            <IconButton
              {...props}
              icon="arrow-right"
              onPress={() => {
                router.push(`/groups/${group.id}`);
              }}
            />
          )}
        />
      ))}
      {group.financialAssets.map((financialAsset) => (
        <Card.Title
          key={financialAsset.id}
          title={financialAsset.type.name}
          subtitle={new Intl.NumberFormat("fr-FR", {
            style: "currency",
            currency: "EUR",
          }).format(getTotalByGroup(group))}
          left={(props) => <Avatar.Icon {...props} icon="file" />}
        />
      ))}
    </ParallaxScrollView>
  );
}

const styles = StyleSheet.create({
  headerImage: {
    color: "#808080",
    top: "50%",
    position: "absolute",
    width: "100%",
    height: 300,
    objectFit: "contain",
  },
  titleContainer: {
    flexDirection: "row",
    gap: 8,
  },
});
