import ParallaxScrollView from "@/components/ParallaxScrollView";
import { ThemedText } from "@/components/ThemedText";
import { ThemedView } from "@/components/ThemedView";
import { IconSymbol } from "@/components/ui/IconSymbol";
import { getFinancialAssetTypes } from "@/utils/get-financial-asset-types";
import { getFinancialAssets } from "@/utils/get-financial-assets";
import { getRandomColor } from "@/utils/get-random-color";
import { getTotalByType } from "@/utils/get-total-by-type";
import { Dimensions, StyleSheet } from "react-native";
import { PieChart } from "react-native-chart-kit";
import { DataTable } from "react-native-paper";

export default function PatrimonyScreen() {
  const financialAssets = getFinancialAssets();
  const conclusions = [
    {
      name: "Disponible",
      financialAssets: financialAssets.filter(
        (financialAsset) =>
          financialAsset.type.isAvailable &&
          !financialAsset.type.isDebt &&
          !financialAsset.type.isTangible
      ),
    },
    {
      name: "Brut",
      financialAssets: financialAssets.filter(
        (financialAsset) =>
          !financialAsset.type.isDebt && !financialAsset.type.isTangible
      ),
    },
    {
      name: "Net",
      financialAssets: financialAssets.filter(
        (financialAsset) =>
          !financialAsset.type.isAvailable &&
          !financialAsset.type.isDebt &&
          !financialAsset.type.isTangible
      ),
    },
    {
      name: "Dettes",
      financialAssets: financialAssets.filter(
        (financialAsset) =>
          financialAsset.type.isDebt && !financialAsset.type.isTangible
      ),
    },
    {
      name: "Réel",
      financialAssets: financialAssets.filter(
        (financialAsset) => !financialAsset.type.isTangible
      ),
    },
    {
      name: "Immobilier",
      financialAssets: financialAssets.filter(
        (financialAsset) => financialAsset.type.isRealEstate
      ),
    },
    {
      name: "Total",
      financialAssets: financialAssets,
    },
  ];

  const piedata2 = getFinancialAssetTypes().map((type) => {
    return {
      name: type.name,
      total: getTotalByType(type),
      color: getRandomColor(),
    };
  });

  const chartConfig = {
    backgroundGradientFrom: "#1E2923",
    backgroundGradientFromOpacity: 0,
    backgroundGradientTo: "#08130D",
    backgroundGradientToOpacity: 0.5,
    color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
    strokeWidth: 2, // optional, default 3
    barPercentage: 0.5,
    useShadowColorFromDataset: false, // optional
  };

  return (
    <ParallaxScrollView
      headerBackgroundColor={{ light: "#D0D0D0", dark: "#353636" }}
      headerImage={
        <IconSymbol
          size={310}
          color="#808080"
          name="chevron.left.forwardslash.chevron.right"
          style={styles.headerImage}
        />
      }
    >
      <ThemedView style={styles.titleContainer}>
        <ThemedText type="title">Patrimoine</ThemedText>
      </ThemedView>

      <PieChart
        data={piedata2}
        width={Dimensions.get("window").width}
        height={150}
        chartConfig={chartConfig}
        accessor={"total"}
        backgroundColor={"transparent"}
        paddingLeft={"15"}
        center={[10, 10]}
        absolute
      />
      <DataTable>
        <DataTable.Header>
          <DataTable.Title>Patrimoine</DataTable.Title>
          <DataTable.Title numeric>Total</DataTable.Title>
          <DataTable.Title numeric>Moyenne</DataTable.Title>
        </DataTable.Header>
        {conclusions.map((conclusion) => {
          return (
            <DataTable.Row key={conclusion.name}>
              <DataTable.Cell
                textStyle={{ width: "100%" }}
                style={{ flex: 1 }}
                numeric
              >
                {conclusion.name} :
              </DataTable.Cell>
              <DataTable.Cell numeric>
                {new Intl.NumberFormat("fr-FR", {
                  style: "currency",
                  currency: "EUR",
                }).format(
                  conclusion.financialAssets.reduce((total, financialAsset) => {
                    return total + financialAsset.total;
                  }, 0)
                )}
              </DataTable.Cell>
              <DataTable.Cell numeric>
                {new Intl.NumberFormat("fr-FR", {
                  style: "currency",
                  currency: "EUR",
                }).format(
                  conclusion.financialAssets.length
                    ? conclusion.financialAssets.reduce(
                        (average, financialAsset) => {
                          return average + financialAsset.average;
                        },
                        0
                      ) / conclusion.financialAssets.length
                    : 0
                )}
              </DataTable.Cell>
            </DataTable.Row>
          );
        })}
      </DataTable>
    </ParallaxScrollView>
  );
}

const styles = StyleSheet.create({
  headerImage: {
    color: "#808080",
    bottom: -90,
    left: -35,
    position: "absolute",
  },
  titleContainer: {
    flexDirection: "row",
    gap: 8,
  },
});
