import ParallaxScrollView from "@/components/ParallaxScrollView";
import { ThemedText } from "@/components/ThemedText";
import { ThemedView } from "@/components/ThemedView";
import { IconSymbol } from "@/components/ui/IconSymbol";
import { getGroupsWithoutParent } from "@/utils/get-platforms";
import { getRandomColor } from "@/utils/get-random-color";
import { getTotalByGroup } from "@/utils/get-total-by-group";
import { router } from "expo-router";
import { Dimensions, StyleSheet } from "react-native";
import { PieChart } from "react-native-chart-kit";
import { Avatar, Card, IconButton } from "react-native-paper";

export default function PlatformScreen() {
  const platforms = getGroupsWithoutParent();

  const piedata = platforms.map((platform) => {
    return {
      name: platform.name,
      total: getTotalByGroup(platform),
      color: getRandomColor(),
    };
  });

  const chartConfig = {
    backgroundGradientFrom: "#1E2923",
    backgroundGradientFromOpacity: 0,
    backgroundGradientTo: "#08130D",
    backgroundGradientToOpacity: 0.5,
    color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
    strokeWidth: 2, // optional, default 3
    barPercentage: 0.5,
    useShadowColorFromDataset: false, // optional
  };

  return (
    <ParallaxScrollView
      headerBackgroundColor={{ light: "#D0D0D0", dark: "#353636" }}
      headerImage={
        <IconSymbol
          size={310}
          color="#808080"
          name="chevron.left.forwardslash.chevron.right"
          style={styles.headerImage}
        />
      }
    >
      <ThemedView style={styles.titleContainer}>
        <ThemedText type="title">Plateformes</ThemedText>
      </ThemedView>

      {platforms.map((platform) => (
        <Card.Title
          key={platform.id}
          title={platform.name}
          subtitle={new Intl.NumberFormat("fr-FR", {
            style: "currency",
            currency: "EUR",
          }).format(getTotalByGroup(platform))}
          left={(props) => <Avatar.Icon {...props} icon="folder" />}
          right={(props) => (
            <IconButton
              {...props}
              icon="arrow-right"
              onPress={() => {
                router.push(`/groups/${platform.id}`);
              }}
            />
          )}
        />
      ))}

      <PieChart
        data={piedata}
        width={Dimensions.get("window").width}
        height={150}
        chartConfig={chartConfig}
        accessor={"total"}
        backgroundColor={"transparent"}
        paddingLeft={"15"}
        center={[10, 10]}
        absolute
      />
    </ParallaxScrollView>
  );
}

const styles = StyleSheet.create({
  headerImage: {
    color: "#808080",
    bottom: -90,
    left: -35,
    position: "absolute",
  },
  titleContainer: {
    flexDirection: "row",
    gap: 8,
  },
});
