import ParallaxScrollView from "@/components/ParallaxScrollView";
import { ThemedText } from "@/components/ThemedText";
import { ThemedView } from "@/components/ThemedView";
import { IconSymbol } from "@/components/ui/IconSymbol";
import { getFinancialAssetTypes } from "@/utils/get-financial-asset-types";
import { getFinancialAssets } from "@/utils/get-financial-assets";
import { getRandomColor } from "@/utils/get-random-color";
import { getTotalByType } from "@/utils/get-total-by-type";
import { StyleSheet } from "react-native";
import { DataTable } from "react-native-paper";

export default function InterestScreen() {
  const financialAssets = getFinancialAssets();
  const conclusions = [
    {
      name: "Intérets Totaux",
      total: financialAssets.reduce((totalInterest, financialAsset) => {
        return totalInterest + financialAsset.totalInterest;
      }, 0),
      style: "currency",
    },
    {
      name: "Intérets Moyens",
      total: financialAssets.length
        ? financialAssets.reduce((averageInterest, financialAsset) => {
            return averageInterest + financialAsset.averageInterest;
          }, 0) / financialAssets.length
        : 0,
      style: "currency",
    },
    {
      name: "Rendement Moyen Annuel",
      total:
        ((financialAssets.length
          ? financialAssets.reduce((averageInterest, financialAsset) => {
              return averageInterest + financialAsset.averageInterest;
            }, 0) / financialAssets.length
          : 0) /
          (financialAssets.length
            ? financialAssets.reduce((average, financialAsset) => {
                return average + financialAsset.average;
              }, 0) / financialAssets.length
            : 0)) *
        12,
      style: "percent",
    },
  ];

  const piedata2 = getFinancialAssetTypes().map((type) => {
    return {
      name: type.name,
      total: getTotalByType(type),
      color: getRandomColor(),
    };
  });

  const chartConfig = {
    backgroundGradientFrom: "#1E2923",
    backgroundGradientFromOpacity: 0,
    backgroundGradientTo: "#08130D",
    backgroundGradientToOpacity: 0.5,
    color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
    strokeWidth: 2, // optional, default 3
    barPercentage: 0.5,
    useShadowColorFromDataset: false, // optional
  };

  return (
    <ParallaxScrollView
      headerBackgroundColor={{ light: "#D0D0D0", dark: "#353636" }}
      headerImage={
        <IconSymbol
          size={310}
          color="#808080"
          name="chevron.left.forwardslash.chevron.right"
          style={styles.headerImage}
        />
      }
    >
      <ThemedView style={styles.titleContainer}>
        <ThemedText type="title">Performance</ThemedText>
      </ThemedView>

      {/* <PieChart
        data={piedata2}
        width={Dimensions.get("window").width}
        height={150}
        chartConfig={chartConfig}
        accessor={"total"}
        backgroundColor={"transparent"}
        paddingLeft={"15"}
        center={[10, 10]}
        absolute
      /> */}
      {conclusions.map((conclusion) => {
        return (
          <DataTable.Row key={conclusion.name}>
            <DataTable.Cell
              textStyle={{ width: "100%" }}
              style={{ flex: 1 }}
              numeric
            >
              {conclusion.name} :
            </DataTable.Cell>
            <DataTable.Cell numeric>
              {new Intl.NumberFormat("fr-FR", {
                style: conclusion.style as any,
                currency: "EUR",
              }).format(conclusion.total)}
            </DataTable.Cell>
          </DataTable.Row>
        );
      })}
    </ParallaxScrollView>
  );
}

const styles = StyleSheet.create({
  headerImage: {
    color: "#808080",
    bottom: -90,
    left: -35,
    position: "absolute",
  },
  titleContainer: {
    flexDirection: "row",
    gap: 8,
  },
});
