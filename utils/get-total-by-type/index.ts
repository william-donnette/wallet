import { getFinancialAssetsByType } from "../get-financial-assets-by-type";

export const getTotalByType = (type: any): number => {
  const financialAssets = getFinancialAssetsByType(type);
  return financialAssets.reduce(
    (total, financialAsset) => total + financialAsset.total,
    0
  );
};
