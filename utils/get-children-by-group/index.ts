import { Groups } from "@/constants/Types";
import data from "@/data.json";
import { getFinancialAssetsByGroup } from "../get-financial-assets-by-group";

export const getChildrenByGroup = (initialGroup: any): Groups => {
  return data.groups
    .filter((group) => group.parent_id === initialGroup.id)
    .map((group) => {
      return {
        ...group,
        financialAssets: getFinancialAssetsByGroup(group),
        parent: initialGroup,
        children: getChildrenByGroup(group),
      };
    });
};
