import { FinancialAssets } from "@/constants/Types";
import { getFinancialAssets } from "../get-financial-assets";

export const getFinancialAssetsByGroup = (
  initialGroup: any
): FinancialAssets => {
  return getFinancialAssets().filter(
    (financialAsset) => financialAsset.group_id === initialGroup.id
  );
};
