import { FinancialAssetType } from "@/constants/Types";
import data from "@/data.json";

export const getFinancialAssetTypes = (): FinancialAssetType[] => {
  return data.financialAssetTypes;
};
