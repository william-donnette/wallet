export const generateRandomColors = (count = 100) => {
  const colors = new Set();

  while (colors.size < count) {
    const randomColor = `#${Math.floor(Math.random() * 16777215)
      .toString(16)
      .padStart(6, "0")}`;
    colors.add(randomColor);
  }

  return Array.from(colors);
};

export const getRandomColor = () => {
  const colorPalette = generateRandomColors(1000);

  const randomIndex = Math.floor(Math.random() * colorPalette.length);
  return colorPalette[randomIndex];
};
