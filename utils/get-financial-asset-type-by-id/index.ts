import { FinancialAssetType } from "@/constants/Types";
import data from "@/data.json";

export const getFinancialAssetTypeById = (
  id: number
): FinancialAssetType | null => {
  return (
    data.financialAssetTypes.find(
      (financialAssetType) => financialAssetType.id === id
    ) ?? null
  );
};
