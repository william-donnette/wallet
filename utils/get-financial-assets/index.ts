import { FinancialAssets } from "@/constants/Types";
import data from "@/data.json";
import { getFinancialAssetTypeById } from "../get-financial-asset-type-by-id";

export const getFinancialAssets = (): FinancialAssets => {
  return data.financialAssets
    .map((financialAsset) => {
      const financials = data.financials
        .filter(
          (financial) => financial.financialAsset_id === financialAsset.id
        )
        .sort((a, b) => {
          const dateA = new Date(a.createdAt);
          const dateB = new Date(b.createdAt);
          return dateA.getTime() - dateB.getTime();
        });
      const type = getFinancialAssetTypeById(financialAsset.type_id);
      if (type) {
        return {
          ...financialAsset,
          total: financials[financials.length - 1]?.total ?? 0,
          average: financials.length
            ? financials.reduce(
                (total, financial) => total + financial.total,
                0
              ) / financials.length
            : 0,
          totalInterest: financials.reduce(
            (total, financial) => total + financial.interest,
            0
          ),
          averageInterest: financials.length
            ? financials.reduce(
                (total, financial) => total + financial.interest,
                0
              ) / financials.length
            : 0,
          savings: financials.length
            ? financials.reduce(
                (total, financial) =>
                  total + financial.inflows - financial.outflows,
                0
              ) / financials.length
            : 0,
          savingRate: financials.length
            ? financials.reduce(
                (average, financial) =>
                  average +
                  (financial.inflows - financial.outflows) / financial.salary,
                0
              ) / financials.length
            : 0,
          financials,
          type,
        };
      }
    })
    .filter(Boolean) as FinancialAssets;
};
