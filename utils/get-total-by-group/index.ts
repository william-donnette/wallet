import { Group } from "@/constants/Types";
import { getChildrenByGroup } from "../get-children-by-group";
import { getFinancialAssetsByGroup } from "../get-financial-assets-by-group";

export const getTotalByGroup = (initialGroup: Group): number => {
  const financialAssets = getFinancialAssetsByGroup(initialGroup);
  const firstTotal = financialAssets.reduce(
    (total, financialAsset) => total + financialAsset.total,
    0
  );
  const children = getChildrenByGroup(initialGroup);
  return children.reduce((total, child) => {
    return total + getTotalByGroup(child);
  }, firstTotal);
};
