import { Groups } from "@/constants/Types";
import { getGroups } from "../get-groups";

export const getGroupsWithoutParent = (): Groups => {
  return getGroups().filter((group) => !group.parent);
};
