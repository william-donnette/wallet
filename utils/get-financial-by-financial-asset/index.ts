import { Financials } from "@/constants/Types";
import data from "@/data.json";

export const getFinancialsByFinancialAsset = (
  financialAsset: any
): Financials => {
  return data.financials
    .filter((financial) => financial.financialAsset_id === financialAsset.id)
    .sort((a, b) => {
      const dateA = new Date(a.createdAt);
      const dateB = new Date(b.createdAt);
      return dateA.getTime() - dateB.getTime();
    });
};
