import { FinancialAssets } from "@/constants/Types";
import { getFinancialAssets } from "../get-financial-assets";

export const getFinancialAssetsByType = (
  financialAssetType: any
): FinancialAssets => {
  return getFinancialAssets().filter(
    (financialAsset) => financialAsset.type_id === financialAssetType.id
  );
};
