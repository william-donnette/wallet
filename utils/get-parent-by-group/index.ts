import { Group } from "@/constants/Types";
import { getGroupById } from "../get-group-by-id";

export const getParentByGroup = (initialGroup: any): Group | null => {
  if (!initialGroup.parent_id) {
    return null;
  }
  return getGroupById(initialGroup.parent_id);
};
