import { Groups } from "@/constants/Types";
import data from "@/data.json";
import { getChildrenByGroup } from "../get-children-by-group";
import { getFinancialAssetsByGroup } from "../get-financial-assets-by-group";
import { getParentByGroup } from "../get-parent-by-group";

export const getGroups = (): Groups => {
  return data.groups.map((group) => {
    if (group.parent_id) {
      return {
        ...group,
        financialAssets: getFinancialAssetsByGroup(group),
        parent: getParentByGroup(group),
        children: getChildrenByGroup(group),
      };
    }
    return {
      ...group,
      financialAssets: getFinancialAssetsByGroup(group),
      parent: null,
      children: getChildrenByGroup(group),
    };
  });
};
