export type Group = {
  id: number;
  parent_id: null | number;
  name: string;
  parent: Group | null;
  children: Groups;
  financialAssets: FinancialAssets;
};

export type Groups = Group[];

export type FinancialAsset = {
  id: number;
  group_id: number;
  type_id: number;
  currency: string;
  updateFrequency: string;
  financials: Financials;
  total: number;
  average: number;
  type: FinancialAssetType;
  totalInterest: number;
  averageInterest: number;
  savings: number;
  savingRate: number;
};

export type FinancialAssets = FinancialAsset[];

export type Financials = Array<{
  financialAsset_id: number;
  createdAt: string;
  total: number;
  inflows: number;
  outflows: number;
  interest: number;
  salary: number;
}>;

export type FinancialAssetType = {
  id: number;
  name: string;
  isAvailable: boolean;
  isDebt: boolean;
  isTangible: boolean;
  isRealEstate: boolean;
};
